#include "Scroll_1d.h++"

Scroll_1d::Scroll_1d(int X, int Y, int W, int H, const char* L) : Fl_Scroll(X, Y, W, H, L) {
	printf("Vertical scroll created - x:%d, y:%d, w:%d, h:%d, l:%s\n", X, Y, W, H, L);
	Fl_Scroll::type(Fl_Scroll::VERTICAL_ALWAYS);
}

void Scroll_1d::type(unsigned char t) {
	switch (t) {
		case Fl_Scroll::VERTICAL:
			Fl_Scroll::type(t); break;
		case Fl_Scroll::HORIZONTAL:
			Fl_Scroll::type(t); break;
		case Fl_Scroll::VERTICAL_ALWAYS:
			Fl_Scroll::type(t); break;
		case Fl_Scroll::HORIZONTAL_ALWAYS:
			Fl_Scroll::type(t); break;
		default:
			printf("Error: Scroll_1d can only be set to one of Fl_Scroll VERTICAL, HORIZONTAL, VERTICAL_ALWAYS, HORIZONTAL_ALWAYS\n");
	}
}

void Scroll_1d::sizeChild(Fl_Widget& o) {
	int offset = this->scrollbar_size();
	if (offset == 0)
		offset = Fl::scrollbar_size();
	
	switch (Fl_Scroll::type()) {
		case Fl_Scroll::VERTICAL:
		case Fl_Scroll::VERTICAL_ALWAYS:
			o.size(this->w() - offset, o.h());
			break;
		case Fl_Scroll::HORIZONTAL:
		case Fl_Scroll::HORIZONTAL_ALWAYS:
			o.size(o.w(), this->h() - offset);
			break;
		default:
			__builtin_trap();
	}
}

int Scroll_1d::findChild(Fl_Widget& o) {
	for (int i = 0; i < children.size(); ++i) {
		if (children[i] == &o)
			return i;
	}
	return -1;
}

void Scroll_1d::positionChild(int i) {
	int nextpos;
	switch (Fl_Scroll::type()) {
		case Fl_Scroll::VERTICAL:
		case Fl_Scroll::VERTICAL_ALWAYS:
			if (i == 0) {
				nextpos = 0;
			} else {
				Fl_Widget* prev = children[i-1];
				nextpos = prev->y() + prev->h();
			}
			children[i]->position(0, nextpos);
			break;
		case Fl_Scroll::HORIZONTAL:
		case Fl_Scroll::HORIZONTAL_ALWAYS:
			if (i == 0) {
				nextpos = 0;
			} else {
				Fl_Widget* prev = children[i-1];
				nextpos = prev->x() + prev->w();
			}
			children[i]->position(nextpos, 0);
			break;
		default:
			__builtin_trap();
	}
	printf("debug: positioned widget %d to %d\n", i, nextpos);
}

void Scroll_1d::add(Fl_Widget& o) {
	Fl_Scroll::add(o);
	sizeChild(o);
	children.push_back(&o);
	positionChild(children.size() - 1);
}

void Scroll_1d::resize(int X, int Y, int W, int H) {
	Fl_Scroll::resize(X, Y, W, H);
	for (int i = 0; i < children.size(); ++i) {
		Fl_Widget* w;
		//try { // maybe helps thread safety idk
			w = children.at(i);
		/*} catch (std::out_of_range) {
			break;
		}*/
		if (w->parent() != this)
			children.erase(children.begin()+i);
		else
			sizeChild(*w);
	}
}