#ifndef Animate_h__
#define Animate_h__

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>


namespace Animation {
	typedef enum {
		LINEAR, EASE, EASE_IN, EASE_OUT, BOUNCE
	} Transition;
	
	typedef enum {
		TRANSLATE, SCALE, COLOR
	} Parameter;
	
	class Quad {
		public:
		int d1, d2, d3, d4;
		Quad(int d1, int d2, int d3, int d4) {
			this->d1=d1; this->d2=d2; this->d3=d3; this->d4=d4;
		}
	};
	
	typedef Quad (*Interpolator)(const Quad&, const Quad&, float);
	
	static Quad interpLinear( const Quad& start, const Quad& end, float progress);
	static Quad interpEase(   const Quad& start, const Quad& end, float progress);
	static Quad interpEaseIn( const Quad& start, const Quad& end, float progress);
	static Quad interpEaseOut(const Quad& start, const Quad& end, float progress);
	static Quad interpBounce( const Quad& start, const Quad& end, float progress);
	
	class Generic {
		Parameter param;
		unsigned long msec;
		Interpolator interp;
		Quad start; Quad end;
		Generic(unsigned int msec, Transition t, const Quad* start, const Quad* end);
		Generic(unsigned int msec, Interpolator interp, const Quad start, const Quad end);
	};
	
	class Translate : Generic {
		public:
		Translate(unsigned int msec, Transition t, int newx, int newy);
		Translate(unsigned int msec, Interpolator interp, int newx, int newy);
	};
	
	class Scale : Generic {
		public:
		Scale(unsigned int msec, Transition t, int neww, int newh);
		Scale(unsigned int msec, Interpolator interp, int neww, int newh);
	};
}


/** All animations are synchronous to avoid unnecessary thread overhead.
 *  FLTK already runs callbacks in a separate thread automatically.
 */
class Animate {
	public:
	static float framerate = 60;
	void apply(Fl_Widget* w, Animation::Generic anim);
};

#endif