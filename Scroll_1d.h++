#ifndef Scroll_1d_h__
#define Scroll_1d_h__

#include <FL/Fl.H>
#include <FL/Fl_Scroll.H>
#include <vector>

class Scroll_1d : public Fl_Scroll {
	public:
	Scroll_1d(int X, int Y, int W, int H, const char* L = 0);
	void type(unsigned char t);
	void add(Fl_Widget& o);
	void add(Fl_Widget* o) {add(*o);};
	void resize(int X, int Y, int W, int H);
	private:
	void sizeChild(Fl_Widget& o);
	int findChild(Fl_Widget& o);
	void positionChild(int i);
	std::vector<Fl_Widget*> children;
	std::vector<int> positions;
};

#endif
