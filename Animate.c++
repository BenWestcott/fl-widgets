#include "Animate.h++"

using namespace Animation;

int interpLinearHelper(int start, int end, float progress) {
	return (float)(end - start)*progress + start;
}
Quad interpLinear(const Quad& start, const Quad& end, float progress) {
	return Quad(
		interpLinearHelper(start.d1, end.d1, progress),
		interpLinearHelper(start.d2, end.d2, progress),
		interpLinearHelper(start.d3, end.d3, progress),
		interpLinearHelper(start.d4, end.d4, progress)
	);
}

Quad interpEase(const Quad& start, const Quad& end, float progress) {
	//TODO
	return Quad(0,0,0,0);
}

Quad interpEaseIn(const Quad& start, const Quad& end, float progress) {
	//TODO
	return Quad(0,0,0,0);
}

Quad interpEaseOut(const Quad& start, const Quad& end, float progress) {
	//TODO
	return Quad(0,0,0,0);
}

Quad interpBounce(const Quad& start, const Quad& end, float progress) {
	//TODO
	return Quad(0,0,0,0);
}


Generic::Generic(unsigned int msec, Transition t, const Quad* start, const Quad* end) {
	this->msec = msec;
	this->start = *start; this->end = *end;
	switch (s) {
		case LINEAR:
			this->interp = interpLinear;
			break;
		case EASE:
			this->interp = interpEase;
			break;
		case EASE_IN:
			this->interp = interpEaseIn;
			break;
		case EASE_OUT:
			this->interp = interpEaseOut;
			break;
		case BOUNCE:
			this->interp = interpBounce;
	}
}

Generic::Generic(unsigned int msec, Interpolator interp, const Quad* start, const Quad* end) {
	this->msec = msec;
	this->start = start; this->end = end;
	this->interp = interp;
}


Translate::Translate(unsigned int msec, Transition t, int newx, int newy) {
	Generic(msec, t, Quad(0, 0, 0, 0), Quad(newx, newy, 0, 0));
	this->param = TRANSLATE;
}
Translate::Translate(unsigned int msec, Interpolator interp, int newx, int newy) {
	Generic(msec, interp, Quad(0, 0, 0, 0), Quad(newx, newy, 0, 0));
	this->param = TRANSLATE;
}

Scale::Scale(unsigned int msec, Transition t, int neww, int newh) {
	Generic(msec, t, Quad(0, 0, 0, 0), Quad(neww, newh, 0, 0));
	this->param = SCALE;
}
Scale::Scale(unsigned int msec, Interpolator interp, int neww, int newh) {
	Generic(msec, interp, Quad(0, 0, 0, 0), Quad(neww, newh, 0, 0));
	this->param = SCALE;
}