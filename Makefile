CC = g++
FLTK_FLAGS = $(shell fltk-config --cxxflags --ldflags)

main.o:
	$(CC) DemoUI.c++ Scroll_1d.c++ $(FLTK_FLAGS)

clean:
	rm -f DemoUI.c++ DemoUI.h++
