# fl-widgets

Helpful custom GUI widgets for use with the [FLTK](https://fltk.org) framework.

While these widgets are licensed as BSD-3, FLTK uses a modified LGPL license.
For details, see https://fltk.org/COPYING.php
